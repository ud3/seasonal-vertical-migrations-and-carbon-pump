%% Creating south and north distribution of C acutus

x_Somalia = [50 53 62 60 50]; %longitude of the polar front
y_Somalia = [5 5 21 21 5];%latitude of the polar front

x_Angola = [13 10 12 12.5 13.5 14 13];
y_Angola = -[27 18 18 22 24 27 27];

%% Make base grid
x = -180:1:179; nx = size(x,2); % [degrees] longitude 
y = -90:1:90; ny = size(y,2); % [degrees] latitude 

[xx,yy] = meshgrid(x,y);

%% Create polygon of the zone
PSomalia = polyshape(x_Somalia, y_Somalia);
inside = isinterior(PSomalia, xx(:),yy(:));
Zone1 = reshape(inside, size(xx,1),size(xx,2));

PAngola = polyshape(x_Angola, y_Angola);
inside = isinterior(PAngola, xx(:),yy(:));
Zone2 = reshape(inside, size(xx,1),size(xx,2));

%Remove land areas
xx2 = mod(xx(:,:,1),360); xx2(xx2>180) = xx2(xx2>180)-360;
land = landmask(yy,xx);

Zone1(land==1)=0; 

%Zone2(land==1)=0;


%% Create abundance map
Z1 = 30000; % [ind / m2] Abundance of individuals per m2 in Somalia
Z2 = 85000; % [ind / m2] Abundance of individuals per m2 in Somalia

Cnatalis = Z1*Zone1+Z2*Zone2; % [ind / m2]

Cnatalis(Cnatalis==0) = NaN;
%Cacutus = Cacutus';
%% Plot abundances
figure
ax = axesm('mercator','Frame','on','MapLatLimit',[-43 45], 'MapLonLimit',[-20 70],'Origin', [0 0 0],'FLineWidth',0.5);
ax.XTick = [-120 -60 0 60 120 180];
ax.YTick = [-40 -20 0 20 40];
box off
axis off
load coast
% geoshow(lat, long,'Color','k')

hold on
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);

surfm(yy,xx,Cnatalis,'AlphaData',~isnan(Cnatalis))
% plotm(y_stf,x_stf,'r')
% plotm(y_ac,x_ac,'k')
colormap('jet')
colorbar


z = 0:100:8000; nz = size(z,2); % [m] depth
dz = 100; % [m] vertical resolution
