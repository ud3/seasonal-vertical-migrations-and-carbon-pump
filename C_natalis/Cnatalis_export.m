Cnatalis_abundance;

ScenarioEX = 1; % small copepods scenario
%  ScenarioEX = 2; % large copepods scenario

% ScenarioDEPTH = 'shallow'; % shallow residency scenario
ScenarioDEPTH = 'deep'; % deep residency scenario

%Value respiration coefficient -- lower than the others because warm water
%copepods and ref temp is 0degrees. Taken to ensure consistency with Verheye et al 2005
b = 3*10^-8; % [mugC ^0.25 s^-1] Metabolic rate coefficient for dormancy
%  b = 2 * 10^-8; % [mugC ^0.25 s^-1] Metabolic rate coefficient for dormancy 
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% Prepare abundances and depths at the right format %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Reshape environmental data
Cnatalis = Cnatalis(:, [181:end,1:180]);
x = 0:1:359;
[xx,yy,zz] = meshgrid(x,y,z);

load('env_data.mat')

long_env = mod(long_env,360);
long_envmodulo = long_env([181:end,1:180]);
Tempmodulo = Temp([181:end,1:180],:,:);
seafloormodulo = seafloor(:,[181:end,1:180]);

[longenv, latenv,depthenv] = meshgrid(long_envmodulo,lat_env, depth);

temp = interp3(longenv, latenv, depthenv, permute(Tempmodulo,[2 1 3]), xx,yy,zz); % [deg C]
seafl = interp2(longenv(:,:,1), latenv(:,:,1), seafloormodulo, xx(:,:,1), yy(:,:,1)); % [m]


%% Choose vertical distribution
if strcmp(ScenarioDEPTH,'shallow')
    diapausedepth = 500; % [m]
    diapausestd = 50; % [m]
elseif strcmp(ScenarioDEPTH,'deep')
    diapausedepth = 750; % [m]
    diapausestd = 100; % [m]
else
    disp("Scenario unknown")
    return
end

zdistri = zeros(ny,nx,nz); % [-] Fraction of the population in each depth bin according to the chosen scenario
for i=1:size(x,2)
    for j=1:size(y,2)
        if ~isnan(seafl(j,i))
            if diapausedepth < seafl(j,i)
                s = gaussmf(z, [diapausestd,diapausedepth]);
                s = s/sum(s); % now s is normalized
            else
                s = (z<seafl(j,i)) & (z>(seafl(j,i)-100)); % if seafloor depth shallower than diapause depth, copepods scatter between 100m above the seafloor and the seafloor
                s = s/sum(s);
            end
            zdistri(j,i,:) = s;
        end
    end
end

%% Now convert ind/m2 to ind/m3 and then respired C/m3 etc
% 
% % Prosome lengths per stage - data from Falk-Petersen 2009, except C6m
% if ScenarioEX == 1
%     p5 = 0.8*2.85; % [mm] total lengths from Brun 2016, and then assuming a ratio 0.8 between total and prosome length
%     p6 = 0.8*3.3;
% elseif ScenarioEX ==2
%     p5 = .8*3.05;
%     p6 = .8*4.6;
% end

% Convert abundances to 3d fields - now in ind / m3
Natalis2 = Cnatalis;
Cnatalis = Cnatalis.*zdistri/dz; % [ind / m3] Concentration of individuals in the water column

%Parameters
% alpha = 4.2; % [mugC mm^-3] Structural mass coefficient
% beta = 7.2; % [mugC mm^-3] Maximum wax ester content coefficient
delta = 0.2; % [-] Minimum wax ester fraction
xi = 0; 
E = 0.6; % [eV] Activation energy coefficient
k = 8.61733*10^-5; % [eV K^-1] Boltzmann constant
T0 = 273; % [K] Reference temperature 
mu = 0.001; % [day^-1] Mortality rate
Dmax = 195; % [days] Maximum diapause duration
% Dmax6 = 45; % [days] Maximum diapause duration
Cegg = 285*0.324*0.5; % [mugC] Total carbon in spawned eggs - multiplied by 0.5 because half the population is males

if ScenarioEX == 1
    % Structural mass
    m5 =  30; % [mugC] Using conversion from Andersen2016 and data from Verheye2005
    % Maximum lipid mass
    w5 = 42; % [mugC]  Max lipid mass of C5 - based on Ohman et al 1989 and carbon to ester ratio of 0.8
elseif ScenarioEX == 2
    % Structural mass
    m5 =  40; % [mugC] Structural mass of C5 
    % Maximum lipid mass
    w5 =  54; % [mugC]  Max lipid mass of C5
end


Tcoeff = exp(E*(temp)./(k*T0*(temp+273))); % [-] temperature coefficient

% Respiration
r = b * (m5)^(3/4) * Tcoeff; % [mugC s^-1 /individual]

% Diapause length
tC = min((1-delta)*w5/(b*Tcoeff*m5^(3/4))/24/3600, Dmax); % [days] Diapause length for C5, based on Visser et al 2016

%% %%%%%%%%%%%%%%%%%%% Respiration rates %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
R =  r.*Cnatalis .* tC * 10^-6*3600*24; % [gC / m3 / yr] Respiration rates due to stage C5

respi = R; % [gC / m3 / yr] total respiration of copepods
% Time = mean((tC5+tC6).*Tonsa,3)./Tonsa2*dz; % [days] Length of diapause at each depth
R = sum((R)*dz,3,'omitnan'); % [gC / m2 / yr] Total respiration rates due to all copepods
%disp(['Max recorded exort is ', num2str(max(max(R))), ' gC /m^2 / yr'])


%% %%%%%%%%%%%%%%%%%%% Mortality rates %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Structural + lipids
mu5 = mu * Cnatalis .* tC .* (m5 + w5 - 0.5 * r .* tC * 3600 * 24) * 10^-6; % [gC / m3 /yr] Mortality rates for C5 at each depth

M = sum((mu5)*dz,3,'omitnan'); % [gC / m2 / yr] Total mortality rates due to all copepods

% After egg laying for females
%mfemale = xi*Tonsa.*exp(-mu*(tC5+tC6)).*max(0,w5+m5-Cegg-r5 .* tC5 * 3600 * 24-r6.*tC6*3600*24)*10^-6; % [gC / m3 /yr] Mortality rates for C6 after having laid eggs

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%% DEGRADATION OF CARCASSES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters
alpha0 = 0.5; % [day^-1] Reference degradation rate at 15 degrees
Tref = 15; % [degrees C] Reference temperature for degradation rate
Q10 = 2; % [-] Q10 coefficient for degradation rate

ALPHA = alpha0 * Q10.^((temp-Tref)/10); % [day^-1] Temperature dependent degradation rate

if ScenarioEX == 1
    u5 = 50; % [m/day] Sinking speed of dead copepods
elseif ScenarioEX == 2
    u5 = 100;
end

% Concentration of carcasses at each depth
d5 = zeros(ny,nx,nz); % [gC / m3] Concentration of carcasses at each depth

for dix = 2:size(z,2) % no need to initialize with the depth index = 1 as there should not be any diapausing copepod there. 
    d5(:,:,dix) = (mu5(:,:,dix)+u5*365/dz*d5(:,:,dix-1))./ (u5*365/dz + ALPHA(:,:,dix)*365);
end

% Resulting DIC production at each depth
dic = (d5) .* ALPHA * 365; % [gC / m3 / yr] Production rate of DIC due to carcasses

%Now need to add the sedimenting particles in the computation - we assume
%that they are respired when they reach the seafloor
for i=1:size(x,2)
    for j=1:size(y,2)
        if ~isnan(seafl(j,i))
            idxbottom = find(isnan(squeeze(dic(j,i,:))),1);         
                       
            dic(j,i,idxbottom-1) = dic(j,i,idxbottom-1) + d5(j,i,idxbottom-1)*u5*365/dz; % added this because we assume that carbon reaching the seafloor is respired
        end
    end
end

%% Compute export
[xq,yq] = meshgrid(x,y);
xq = mod(xq,360);
DLON = 0*xq+1;
DLAT = 0*yq+1;
DX = (2*pi*6371e3/360)*DLON.*cos(deg2rad(yq))*(x(2)-x(1));
DY = (2*pi*6371e3/360)*DLAT*(y(2)-y(1));
Area = DX.*DY; % m^2

expmort =sum(sum( sum(dic,3,'omitnan')*dz.*Area,'omitnan'),'omitnan') /1e15; % [PgC / yr]
exprespi =sum(sum (R.*Area,'omitnan'),'omitnan') /1e15; % [PgC/yr]
% (w5+m5-Cegg-r5.*tC5*3600*24-r6.*tC6*3600*24)>0
% mu.*(tC5+tC6).*exp(-mu*(tC5+tC6))./(1-exp(-mu*(tC5+tC6)))
% eggslaid = sum(sum(sum(Tonsa.*exp(-mu*(tC5+tC6)).*min(Cegg,(w5+m5-r5.*tC5*3600*24-r6.*tC6*3600*24))*10^-6/1e15,3,'omitnan')*dz.*Area,'omitnan'),'omitnan'); % [PgC / yr] eggs laid per year, just to check that Biomass ~= mort + respi + eggslaid
totbio = sum(sum( sum(Cnatalis,3,'omitnan')*dz.*Area,'omitnan'),'omitnan') /1e15*(w5+m5)*10^-6; % [PgC] Total biomass at the beginning of diapause

% if we want to compute the area
% mask = R>0;
% sum(sum (mask.*Area,'omitnan'),'omitnan')

%% Plot export

x = 0:1:359; nx = size(x,2); % [degrees] longitude 
y = -90:1:90; ny = size(y,2); % [degrees] latitude 

[xx,yy] = meshgrid(x,y);


EXPmort = sum(dic,3,'omitnan')*dz; % [gC/m2/yr]
EXPrespi= R; % [gC/m2/yr]

EXP = EXPmort + EXPrespi;
EXP(EXP==0) = NaN;

EXPmort(EXPmort<0.01) = NaN;
EXPrespi(EXPrespi==0) = NaN;

figure
ax = axesm('mercator','Frame','on','MapLatLimit',[-43 45], 'MapLonLimit',[-20 70],'Origin', [0 0 0],'FLineWidth',0.5);
ax.XTick = [-120 -60 0 60 120 180];
ax.YTick = [-40 -20 0 20 40];
box off
axis off
load coast
% geoshow(lat, long,'Color','k')

hold on
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);

surfm(yy,xx,EXP,'AlphaData',~isnan(EXP))
% plotm(y_stf,x_stf,'r')
% plotm(y_ac,x_ac,'k')
colormap('jet')
colorbar
