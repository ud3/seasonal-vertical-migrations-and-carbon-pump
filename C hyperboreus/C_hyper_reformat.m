ScenarioEX = 1; % small export scenario
% ScenarioEX = 2; % large export scenario

ScenarioDEPTH = 'shallow'; % shallow residency scenario
% ScenarioDEPTH = 'deep'; % deep residency scenario

% b = 5 * 10^-7; % [mugC ^0.25 s^-1] Metabolic rate coefficient for dormancy 
b = 2.5 * 10^-7; % [mugC ^0.25 s^-1] Metabolic rate coefficient for dormancy    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% Prepare abundances and depths at the right format %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Make base grid
x = 0:1:359; nx = size(x,2); % [degrees] longitude 
y = -90:1:90; ny = size(y,2); % [degrees] latitude 
z = 0:100:8000; nz = size(z,2); % [m] depth
dz = 100; % [m] vertical resolution
[xx,yy,zz] = meshgrid(x,y,z);


%% Reshape abundance data
F = scatteredInterpolant(mod(C3lon,360),C3lat,C3ab,'linear','none');
c3 = F(xx(:,:,1),yy(:,:,1)); % [log_e (ind/m2)] That's the unit for now, change later

F = scatteredInterpolant(mod(C4lon,360),C4lat,C4ab,'linear','none');
c4 = F(xx(:,:,1),yy(:,:,1));

F = scatteredInterpolant(mod(C5lon,360),C5lat,C5ab,'linear','none');
c5 = F(xx(:,:,1),yy(:,:,1));

F = scatteredInterpolant(mod(C6flon,360),C6flat,C6fab,'linear','none');
c6f = F(xx(:,:,1),yy(:,:,1));

F = scatteredInterpolant(mod(C6mlon,360),C6mlat,C6mab,'linear','none');
c6m = F(xx(:,:,1),yy(:,:,1));

%% Reshape environmental data
load('env_data.mat')
long_env = mod(long_env,360);
long_envmodulo = long_env([181:end,1:180]);
Tempmodulo = Temp([181:end,1:180],:,:);
seafloormodulo = seafloor(:,[181:end,1:180]);

[longenv, latenv,depthenv] = meshgrid(long_envmodulo,lat_env, depth);

temp = interp3(longenv, latenv, depthenv, permute(Tempmodulo,[2 1 3]), xx,yy,zz); % [deg C]
seafl = interp2(longenv(:,:,1), latenv(:,:,1), seafloormodulo, xx(:,:,1), yy(:,:,1)); % [m]

%% Change units abundance
c3  = exp(c3); % [ind/m2]
c4  = exp(c4);
c5  = exp(c5);
c6f = exp(c6f);
c6m = exp(c6m);

c3(isnan(seafl)) = NaN;
c4(isnan(seafl)) = NaN;
c5(isnan(seafl)) = NaN;
c6f(isnan(seafl)) = NaN;
c6m(isnan(seafl)) = NaN;

%% Choose vertical distribution
if strcmp(ScenarioDEPTH,'shallow')
    diapausedepth = 600; % [m] Scenario 1: 600m or 20m above seafloor + std = 50 / scenario 2: 1500m or 20m above seafloor + std = 100
    diapausestd = 50; % [m]
elseif strcmp(ScenarioDEPTH,'deep')
    diapausedepth = 1500; % [m]
    diapausestd = 100; % [m]
else
    disp("Scenario unknown")
    return
end

zdistri = zeros(ny,nx,nz); % [-] Fraction of the population in each depth bin according to the chosen scenario
for i=1:size(x,2)
    for j=1:size(y,2)
        if ~isnan(seafl(j,i))
            if diapausedepth < seafl(j,i)
                s = gaussmf(z, [diapausestd,diapausedepth]);
                s = s/sum(s); % now s is normalized
            else
                s = (z<seafl(j,i)) & (z>(seafl(j,i)-100)); % if seafloor depth shallower than diapause depth, copepods scatter between 100m above the seafloor and the seafloor
                s = s/sum(s);
            end
            zdistri(j,i,:) = s;
        end
    end
end

%% Now convert ind/m2 to ind/m3 and then respired C/m3 etc

% Prosome lengths per stage - data from Falk-Petersen 2009, except C6m
if ScenarioEX == 1
    p3 = 2.47; % [mm]
    p4 = 3.4;
    p5 = 4.5;
    p6f = 6.2;
    p6m = 6;
elseif ScenarioEX ==2
    p3 = 2.47;
    p4 = 4.0;
    p5 = 6.0;
    p6f = 7.0;
    p6m = 6.5;
end

% Convert abundances to 3d fields - now in ind / m3
c3 = c3.*zdistri/dz; % [ind / m3] Concentration of individuals in the water column
c4 = c4.*zdistri/dz; % [ind / m3] Concentration of individuals in the water column
c5 = c5.*zdistri/dz; % [ind / m3] Concentration of individuals in the water column
c6f = c6f.*zdistri/dz; % [ind / m3] Concentration of individuals in the water column
c6m = c6m.*zdistri/dz; % [ind / m3] Concentration of individuals in the water column

%Parameters
alpha = 4.2; % [mugC mm^-3] Structural mass coefficient
beta = 7.2; % [mugC mm^-3] Maximum wax ester content coefficient
delta = 0.2; % [-] Minimum wax esther fraction
xi = 0.5; 
E = 0.6; % [eV] Activation energy coefficient
k = 8.61733*10^-5; % [eV K^-1] Boltzmann constant
T0 = 273; % [K] Reference temperature 
mu = 0.001; % [day^-1] Mortality rate
Dmax = 240; % [days] Maximum diapause duration
Cegg = 900; % [mugC] Total carbon in spawned eggs

Tcoeff = exp(E*(temp)./(k*T0*(temp+273))); % [-] temperature coefficient

% Respiration per stages
r3 = b * (alpha * p3^3)^(3/4) * Tcoeff;   % [mugC s^-1 /individual]
r4 = b * (alpha * p4^3)^(3/4) * Tcoeff;   % [mugC s^-1 /individual]
r5 = b * (alpha * p5^3)^(3/4) * Tcoeff;   % [mugC s^-1 /individual]
r6f = b * (alpha * p6f^3)^(3/4) * Tcoeff; % [mugC s^-1 /individual]
r6m = b * (alpha * p6m^3)^(3/4) * Tcoeff; % [mugC s^-1 /individual]

% Structural mass
m3 =  alpha * p3^3; % [mugC] Structural mass of C3
m4 =  alpha * p4^3; % [mugC] Structural mass of C4
m5 =  alpha * p5^3; % [mugC] Structural mass of C5
m6f = alpha * p6f^3; % [mugC] Structural mass of C6f
m6m = alpha * p6m^3; % [mugC] Structural mass of C6m

% Maximum lipid mass
w3 =  beta * p3^3; % [mugC]  Max lipid mass of C3
w4 =  beta * p4^3; % [mugC] Max lipid mass of C4
w5 =  beta * p5^3; % [mugC] Max lipid mass of C5
w6f = beta * p6f^3; % [mugC] Max lipid mass of C6f
w6m = beta * p6m^3; % [mugC] Max lipid mass of C6m

% Diapause length
tC3 = min((1-delta)*w3/(b*Tcoeff*m3^(3/4))/24/3600, Dmax); % [days] Diapause length for C3, based on Visser et al 2016
tC4 = min((1-delta)*w4/(b*Tcoeff*m4^(3/4))/24/3600, Dmax); % [days] Diapause length for C4, based on Visser et al 2016
tC5 = min((1-delta)*w5/(b*Tcoeff*m5^(3/4))/24/3600, Dmax); % [days] Diapause length for C5, based on Visser et al 2016
tC6f = min((1-delta)*w6f/(b*Tcoeff*m6f^(3/4))/24/3600, Dmax); % [days] Diapause length for C6f, based on Visser et al 2016
tC6m = min((1-delta)*w6m/(b*Tcoeff*m6m^(3/4))/24/3600, Dmax); % [days] Diapause length for C6m, based on Visser et al 2016

%% %%%%%%%%%%%%%%%%%%% Respiration rates %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
R3 =  r3.*c3 .* tC3   * 10^-6*3600*24; % [gC / m3 / yr] Respiration rates due to stage C3
R4 =  r4.*c4 .* tC4   * 10^-6*3600*24; % [gC / m3 / yr] Respiration rates due to stage C4
R5 =  r5.*c5 .* tC5   * 10^-6*3600*24; % [gC / m3 / yr] Respiration rates due to stage C5
R6f = r6f.*c6f.* tC6f * 10^-6*3600*24; % [gC / m3 / yr] Respiration rates due to stage C6f
R6m = r6m.*c6m.* tC6m * 10^-6*3600*24; % [gC / m3 / yr] Respiration rates due to stage C6m

respi = R3+R4+R5+R6f+R6m; % [gC / m3 / yr] total respiration of copepods

R = sum((R3+R4+R5+R6f+R6m)*dz,3,'omitnan'); % [gC / m2 / yr] Total respiration rates due to all copepods
%disp(['Max recorded exort is ', num2str(max(max(R))), ' gC /m^2 / yr'])


%% %%%%%%%%%%%%%%%%%%% Mortality rates %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Structural + lipids
mu3 = mu *  c3 .*  tC3  .* (m3  + w3  - 0.5 * r3  .* tC3  * 3600 * 24) * 10^-6; % [gC / m3 /yr] Mortality rates for C3 at each depth
mu4 = mu *  c4 .*  tC4  .* (m4  + w4  - 0.5 * r4  .* tC4  * 3600 * 24) * 10^-6; % [gC / m3 /yr] Mortality rates for C4 at each depth
mu5 = mu *  c5 .*  tC5  .* (m5  + w5  - 0.5 * r5  .* tC5  * 3600 * 24) * 10^-6; % [gC / m3 /yr] Mortality rates for C5 at each depth
mu6f = mu * c6f .* tC6f .* (m6f + w6f - 0.5 * r6f .* tC6f * 3600 * 24) * 10^-6; % [gC / m3 /yr] Mortality rates for C6f at each depth
mu6m = mu * c6m .* tC6m .* (m6m + w6m - 0.5 * r6m .* tC6m * 3600 * 24) * 10^-6; % [gC / m3 /yr] Mortality rates for C6m at each depth

M = sum((mu3+mu4+mu5+mu6f+mu6m)*dz,3,'omitnan'); % [gC / m2 / yr] Total mortality rates due to all copepods

% After egg laying for females
mfemale = xi*c6f*mu.*tC6f.*exp(-mu*tC6f)./(1-exp(-mu*tC6f)).*max(0,w6f+m6f-Cegg-r6f.*tC6f*3600*24)*10^-6; % [gC / m3 /yr] Mortality rates for C6f after having laid eggs

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%% DEGRADATION OF CARCASSES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters
alpha0 = 0.5; % [day^-1] Reference degradation rate at 15 degrees
Tref = 15; % [degrees C] Reference temperature for degradation rate
Q10 = 2; % [-] Q10 coefficient for degradation rate

ALPHA = alpha0 * Q10.^((temp-Tref)/10); % [day^-1] Temperature dependent degradation rate

if ScenarioEX == 1
    u3 = 50; % [m/day] Sinking speed of dead copepods
    u4 = 100;
    u5 = 200;
    u6f = 250;
    u6m = 200;
    u6fegg = 300; %this one is for females dying after laying eggs - we assume that they are depleted of lipids so they will sink faster
elseif ScenarioEX == 2
    u3 = 100;
    u4 = 200;
    u5 = 500;
    u6f = 800;
    u6m = 600;
    u6fegg = 1000;
end

% Concentration of carcasses at each depth
d3 = zeros(ny,nx,nz); % [gC / m3] Concentration of carcasses at each depth
d4 = d3; d5 = d3; d6f = d3; d6m = d3; d6fegg = d3; 

for dix = 2:size(z,2) % no need to initialize with the depth index = 1 as there should not be any diapausing copepod there. 
    d3(:,:,dix) = (mu3(:,:,dix)+u3*365/dz*d3(:,:,dix-1))./ (u3*365/dz + ALPHA(:,:,dix)*365); % [gC / m3]
    d4(:,:,dix) = (mu4(:,:,dix)+u4*365/dz*d4(:,:,dix-1))./ (u4*365/dz + ALPHA(:,:,dix)*365);
    d5(:,:,dix) = (mu5(:,:,dix)+u5*365/dz*d5(:,:,dix-1))./ (u5*365/dz + ALPHA(:,:,dix)*365);
    d6f(:,:,dix) = (mu6f(:,:,dix)+u6f*365/dz*d6f(:,:,dix-1))./ (u6f*365/dz + ALPHA(:,:,dix)*365);
    d6m(:,:,dix) = (mu6m(:,:,dix)+u6m*365/dz*d6m(:,:,dix-1))./ (u6m*365/dz + ALPHA(:,:,dix)*365);
    d6fegg(:,:,dix) = (mfemale(:,:,dix)+u6fegg*365/dz*d6fegg(:,:,dix-1))./ (u6fegg*365/dz + ALPHA(:,:,dix)*365);
end

% Resulting DIC production at each depth
dic = (d3+d4+d5+d6f+d6m+d6fegg) .* ALPHA * 365; % [gC / m3 / yr] Production rate of DIC due to carcasses

%Now need to add the sedimenting particles in the computation - we assume
%that they are respired when they reach the seafloor
for i=1:size(x,2)
    for j=1:size(y,2)
        if ~isnan(seafl(j,i))
            idxbottom = find(isnan(squeeze(dic(j,i,:))),1);         
                       
            dic(j,i,idxbottom-1) = dic(j,i,idxbottom-1) + d3(j,i,idxbottom-1)*u3*365/dz +d4(j,i,idxbottom-1)*u4*365/dz+d5(j,i,idxbottom-1)*u5*365/dz +d6f(j,i,idxbottom-1)*u6f*365/dz+d6m(j,i,idxbottom-1)*u6m*365/dz +d6fegg(j,i,idxbottom-1)*u6fegg*365/dz; % added this because we assume that carbon reaching the seafloor is respired
        end
    end
end

%% Compute export
[xq,yq] = meshgrid(x,y);
xq = mod(xq,360);
DLON = 0*xq+1;
DLAT = 0*yq+1;
DX = (2*pi*6371e3/360)*DLON.*cos(deg2rad(yq))*(x(2)-x(1));
DY = (2*pi*6371e3/360)*DLAT*(y(2)-y(1));
Area = DX.*DY; % m^2

expmort =sum(sum( sum(dic,3,'omitnan')*dz.*Area,'omitnan'),'omitnan') /1e15; % [PgC / yr]
exprespi =sum(sum (R.*Area,'omitnan'),'omitnan') /1e15; % [PgC/yr]

totbio3 = sum(sum( sum(c3,3,'omitnan')*dz.*Area,'omitnan'),'omitnan') /1e15*(w3+m3)*10^-6; % [PgC] Total biomass at the beginning of diapause
totbio4 = sum(sum( sum(c4,3,'omitnan')*dz.*Area,'omitnan'),'omitnan') /1e15*(w4+m4)*10^-6; % [PgC] Total biomass at the beginning of diapause
totbio5 = sum(sum( sum(c5,3,'omitnan')*dz.*Area,'omitnan'),'omitnan') /1e15*(w5+m5)*10^-6; % [PgC] Total biomass at the beginning of diapause
totbio6m = sum(sum( sum(c6m,3,'omitnan')*dz.*Area,'omitnan'),'omitnan') /1e15*(w6m+m6m)*10^-6; % [PgC] Total biomass at the beginning of diapause
totbio6f = sum(sum( sum(c6f,3,'omitnan')*dz.*Area,'omitnan'),'omitnan') /1e15*(w6f+m6f)*10^-6; % [PgC] Total biomass at the beginning of diapause
totbio = totbio3 + totbio4 + totbio5 + totbio6m + totbio6f;
% if we want to compute the area
% mask = R>0;
% sum(sum (mask.*Area,'omitnan'),'omitnan')