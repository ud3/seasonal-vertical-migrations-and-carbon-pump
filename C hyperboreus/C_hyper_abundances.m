% C hyperboreus
A = readtable('Chyperboreus.csv');
% Abundance units are in log_e (ind.m^-2)

C3lon = A.lon(strcmp(A.stage,'Chypc3'));
C3lat = A.lat(strcmp(A.stage,'Chypc3'));
C3ab = A.pred(strcmp(A.stage,'Chypc3'));
C3 = [C3lon C3lat C3ab];

C4lon = A.lon(strcmp(A.stage,'Chypc4'));
C4lat = A.lat(strcmp(A.stage,'Chypc4'));
C4ab = A.pred(strcmp(A.stage,'Chypc4'));
C4 = [C4lon C4lat C4ab];

C5lon = A.lon(strcmp(A.stage,'Chypc5'));
C5lat = A.lat(strcmp(A.stage,'Chypc5'));
C5ab = A.pred(strcmp(A.stage,'Chypc5'));
C5 = [C5lon C5lat C5ab];

C6mlon = A.lon(strcmp(A.stage,'Chypc6m'));
C6mlat = A.lat(strcmp(A.stage,'Chypc6m'));
C6mab = A.pred(strcmp(A.stage,'Chypc6m'));
C6m = [C6mlon C6mlat C6mab];

C6flon = A.lon(strcmp(A.stage,'Chypc6f'));
C6flat = A.lat(strcmp(A.stage,'Chypc6f'));
C6fab = A.pred(strcmp(A.stage,'Chypc6f'));
C6f = [C6flon C6flat C6fab];


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure
subplot(231)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
scatterm(C3(:,2),C3(:,1),2,C3(:,3))
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([min(A.pred) max(A.pred)])
colormap('jet')
% colorbar

subplot(232)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
scatterm(C4(:,2),C4(:,1),2,C4(:,3))
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([min(A.pred) max(A.pred)])
% colorbar

subplot(233)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
scatterm(C5(:,2),C5(:,1),2,C5(:,3))
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([min(A.pred) max(A.pred)])
% colorbar

subplot(234)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
scatterm(C6f(:,2),C6f(:,1),2,C6f(:,3))
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([min(A.pred) max(A.pred)])
% colorbar

subplot(235)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
scatterm(C6m(:,2),C6m(:,1),2,C6m(:,3))
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([min(A.pred) max(A.pred)])
% colorbar

subplot(236)
caxis([min(A.pred) max(A.pred)])
colorbar

%%
figure
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
scatterm(C6m(:,2),C6m(:,1),4,(exp(C3(:,3))+exp(C4(:,3))+exp(C5(:,3))+exp(C6f(:,3))+exp(C6m(:,3))))
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
% caxis([min(A.pred) max(A.pred)])
colorbar
title('C hyper abundance [ln(ind / m^2)]')
