figure
subplot(231)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
surfm(yy(:,:,1),xx(:,:,1),c3)
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([min(A.pred) max(A.pred)])
colormap('jet')
% colorbar

subplot(232)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
surfm(yy(:,:,1),xx(:,:,1),c4)
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([min(A.pred) max(A.pred)])
colormap('jet')

subplot(233)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
surfm(yy(:,:,1),xx(:,:,1),c5)
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([min(A.pred) max(A.pred)])
colormap('jet')

subplot(234)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
surfm(yy(:,:,1),xx(:,:,1),c6f)
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([min(A.pred) max(A.pred)])
colormap('jet')

subplot(235)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
surfm(yy(:,:,1),xx(:,:,1),c6m)
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([min(A.pred) max(A.pred)])
colormap('jet')