% plot respiration rates of copepods at depth
%units are gC / m2 / yr
figure
subplot(231)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
surfm(yy(:,:,1),xx(:,:,1),sum((R3)*dz,3,'omitnan'))
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
% caxis([min(A.pred) max(A.pred)])
colormap('jet')
colorbar

subplot(232)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
surfm(yy(:,:,1),xx(:,:,1),sum((R4)*dz,3,'omitnan'))
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([0 8])
colormap('jet')
colorbar

subplot(233)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
surfm(yy(:,:,1),xx(:,:,1),sum((R5)*dz,3,'omitnan'))
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
% caxis([min(A.pred) max(A.pred)])
colormap('jet')
colorbar

subplot(234)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
surfm(yy(:,:,1),xx(:,:,1),sum((R6f)*dz,3,'omitnan'))
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([0 8])
colormap('jet')
colorbar

subplot(235)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
surfm(yy(:,:,1),xx(:,:,1),sum((R6m)*dz,3,'omitnan'))
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([0 8])
colormap('jet')
colorbar

subplot(236)
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
% geoshow(lat, long,'Color','k')
% surfm(LAT', LON', cc,'AlphaData',~isnan(cc));
surfm(yy(:,:,1),xx(:,:,1),R)
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([0 15])
colormap('jet')
colorbar