figure
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
ax.XTick = [-120 -60 0 60 120 180];
ax.YTick = [-40 -20 0 20 40];
% objects = [handlem('grid'); handlem('mlabel'); handlem('plabel')];
% set(objects,'Clipping','on');
DICtoplot = sum(dic,3,'omitnan')*dz;
DICtoplot = [DICtoplot, DICtoplot(:,1)];
xtoplot = [x, 360];
box off
axis off
load coast
geoshow(lat, long,'Color','k')
surfm(y, xtoplot,DICtoplot ,'AlphaData',~isnan(DICtoplot));%,'EdgeColor','none')
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
colormap('jet')
colorbar
% 
% figure %% this is the same figure - plotting the mortality rates and not the DIC / ultimately should be the same as all carcasses end up transformed in DIC
% ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% ax.XTick = [-120 -60 0 60 120 180];
% ax.YTick = [-40 -20 0 20 40];
% % objects = [handlem('grid'); handlem('mlabel'); handlem('plabel')];
% % set(objects,'Clipping','on');
% box off
% axis off
% load coast
% geoshow(lat, long,'Color','k')
% surfm(y, x,M+ sum(mfemale,3,'omitnan')*dz,'AlphaData',~isnan(dic(:,:,1)));%,'EdgeColor','none')
% geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
% colormap('jet')
% colorbar