figure
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% DICtoplot = [DICtoplot, DICtoplot(:,1)];
Rtoplot = [R, R(:,1)];
xtoplot = [x, 360];
box off
axis off
load coast
surfm(y,xtoplot,Rtoplot+DICtoplot)
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
caxis([0 15])
colormap('jet')
colorbar