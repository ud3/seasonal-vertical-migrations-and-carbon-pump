%% Extract 2000m bathy line close to Antarctica
S = shaperead("ne_10m_bathymetry_I_2000.shp");

% mapshow(S)
%%
s1 = [S(156).X; S(156).Y];
s2 = [S(161).X; S(161).Y];
s3 = [S(165).X; S(165).Y];
s4 = [S(191).X; S(191).Y];
s5 = [S(198).X; S(198).Y];
%%
figure
plot(s1(1,64:740),s1(2,64:740))
hold on
plot(s2(1,578:1474),s2(2,578:1474))
plot(s3(1,245:1952),s3(2,245:1952))
plot(s4(1,926:1854),s4(2,926:1854))
plot(s5(1,5283:6032),s5(2,5283:6032))

%%
SX = [fliplr(s4(1,926:1854)), fliplr(s3(1,245:1952)), fliplr(s2(1,578:1474)), fliplr(s1(1,64:740)),  fliplr(s5(1,5283:6032))];
SY = [fliplr(s4(2,926:1854)), fliplr(s3(2,245:1952)), fliplr(s2(2,578:1474)), fliplr(s1(2,64:740)),  fliplr(s5(2,5283:6032))];
%SX = mod(SX,360);
figure, plot(SX,SY)

