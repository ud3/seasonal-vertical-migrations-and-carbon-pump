%% Creating south and north distribution of C acutus

x_pf = [-180, -160, -110, -60 -10, 0, 50, 75, 100, 150, 180]; %longitude of the polar front
y_pf = -[58 60 58 56 50 50 52 48 52 55 58];%latitude of the polar front

addpath ../bathy
Get_2000m_isobath
%%Run Get_2000m_isobath to get southern bound
%% Make base grid
x = -180:1:179; nx = size(x,2); % [degrees] longitude 
y = -90:1:90; ny = size(y,2); % [degrees] latitude 

[xx,yy] = meshgrid(x,y);

%% Create polygon of the zone

XX = [SX, fliplr(x_pf), SX(1)];
YY = [SY, fliplr(y_pf), SY(1)];

PP = polyshape(XX,YY);

inside = isinterior(PP, xx(:),yy(:));
Zone = reshape(inside, size(xx,1),size(xx,2));

%Remove land areas
xx2 = mod(xx(:,:,1),360); xx2(xx2>180) = xx2(xx2>180)-360;
land = landmask(yy,xx);

Zone(land==1)=0; 


%% Create abundance map
Z1 = 1000; % [ind / m2] Abundance of individuals per m2 in zone 1

Cacutus = Z1*Zone; % [ind / m2]

Cacutus(Cacutus==0) = NaN;
%Cacutus = Cacutus';
%% Plot abundances
figure
ax = axesm('eqdazim','Frame','on','MapLatLimit',[-90 -10],'Origin', [-90 0 0],'FLineWidth',0.5);
ax.XTick = [-120 -60 0 60 120 180];
ax.YTick = [-40 -20 0 20 40];
box off
axis off
load coast
% geoshow(lat, long,'Color','k')

hold on
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);

surfm(yy,xx,Cacutus,'AlphaData',~isnan(Cacutus))
% plotm(y_stf,x_stf,'r')
% plotm(y_ac,x_ac,'k')
colormap('jet')
colorbar


z = 0:100:8000; nz = size(z,2); % [m] depth
dz = 100; % [m] vertical resolution
