figure
ax = axesm('eqdazim','Frame','on','MapLatLimit',[50 90],'Origin', [90 0 0],'FLineWidth',0.5);
% DICtoplot = [DICtoplot, DICtoplot(:,1)];
Rtoplot = Respi + Mort;
% Rtoplot = tC(:,:,10);
Rtoplot = [Rtoplot, Rtoplot(:,1)];
xtoplot = [x, 360];
box off
axis off
load coast
surfm(y,xtoplot,Rtoplot)
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
% caxis([0 15])
colormap('jet')
colorbar

%%
figure
exptoplot = [Respi+Mort,Respi(:,end)+Mort(:,end)];
extoplot(:,1) = exptoplot(:,end);
exptoplot(exptoplot == 0) = NaN;
ax = axesm('mollweid','MapProjection','mercator','Frame','on','MapLonLimit',[-60 10],'MapLatLimit', [53 67],'Origin', [0 -160 0],'FLineWidth',0.5);
surfm(y,[x,0],exptoplot,'AlphaData',~isnan(exptoplot)), colorbar
box off
axis off
load coast
geoshow(lat, long,'Color','k')
ax.XTick = [-60 -30 0];
ax.YTick = [53 60 67];


%%
figure
ax = axesm('mollweid','MapProjection','mercator','Frame','on','MapLonLimit',[-60 10],'MapLatLimit', [53 69],'Origin', [0 -160 0],'FLineWidth',0.5);
Rtoplot = Respi + Mort;
% Rtoplot = tC(:,:,10);
Rtoplot = [Rtoplot, Rtoplot(:,1)];
xtoplot = [x, 360];
box off
axis off
load coast
surfm(y,[x,0],exptoplot,'AlphaData',~isnan(exptoplot))
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
% caxis([0 15])
colormap('jet')
colorbar


