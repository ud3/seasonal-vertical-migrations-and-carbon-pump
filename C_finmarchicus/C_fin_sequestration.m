% % Code developed by Jerome Pinti
% % For questions and remarks, please email pintijerome@gmail.com
% % This code is based on the transport matrix method developed by Tim
% % DeVries from UCSB. The transport matrix can be downloaded there: https://tdevries.eri.ucsb.edu/models-and-data-products/
% % References to the transport matrix are DeVries and Primeau 2011 and DeVries 2014.

%% Choose the input
Q = dic + R;

%% Load the transport matrix
addpath C:\Users\jppi\Documents\MATLAB\SVM_sequestration
load('91x180x48_omega3_Kvvariable_geoLuc_AH2e5.mat') %high resolution - ~10 minutes to inverse

% load('CTL.mat') % lower resolution - less than a minute

%% Make base grid
% [lonq,latq,zq] = meshgrid(x,y,z);

%% Preparation of the transport matrix
grid = output.grid;
TR = output.TR; % yr^-1
msk = output.msk;
M3d = output.M3d; % land = 0. ocean = 1


VOL = grid.DXT3d.*grid.DYT3d.*grid.DZT3d;
V = VOL(msk.pkeep);

m = size(TR,1);
sink = zeros(m,1);
sink(1:length(msk.hkeep)) = 1e10; % instantaneous Surface SINK
SSINK = spdiags(sink,0,m,m);
A = TR-SSINK; % transport + sink in surface
tic
dA = decomposition(A); %if we want it to go faster
toc
%%

        Q = permute(Q, [2 1 3]);
        q_ocim = interp3(xx,yy,zz,permute(Q, [2 1 3]),grid.XT3d,grid.YT3d,grid.ZT3d);
               
        q_ocim = q_ocim(msk.pkeep);
        q_ocim(isnan(q_ocim)) = 0;
        q_ocim = q_ocim*(expmort+exprespi)/(V'*q_ocim / 1e15); % reformat to correct bias of interp3 that generally decreases flux
        export = V'*q_ocim / 1e15; % [PgC / yr]
        tic
        cseq = -dA \q_ocim;
        toc
        TotCseq = V'*cseq / 1e15
        seqime = TotCseq / export

% Re interpolate to Q
qocim = 0*M3d+NaN; % make a 3-d array of NaN
qocim(msk.pkeep) = q_ocim;
qocim(isnan(qocim)) = 0;

C_eq = 0*M3d+NaN;
C_eq(msk.pkeep) = cseq;
C_eq(isnan(C_eq)) = 0;


%% A simple plot
LAT = repmat(y,nx+1,1);
LON = repmat([x,360]',1,ny);
cc = sum(C_eq.*grid.DZT3d,3); cc = [cc, cc(:,end)]; % gC/m2

figure
cc(cc<0) = 0;
ax = axesm('mollweid','Frame','on','MapLatLimit',[-90 90],'Origin', [0 -160 0],'FLineWidth',0.5);
ax.XTick = [-120 -60 0 60 120 180];
ax.YTick = [-40 -20 0 20 40];
% objects = [handlem('grid'); handlem('mlabel'); handlem('plabel')];
% set(objects,'Clipping','on');
box off
axis off
load coast
geoshow(lat, long,'Color','k')

surfm(LAT', LON', cc,'AlphaData',~isnan(cc));%,'EdgeColor','none')
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
% plotm(yred,xred,'r')
colorbar
colormap('jet')
%%
locexp = (expmort + exprespi)*10^15 / 2.9 / 10^12 % [gC / m2 / yr] Mean export 2.9 for masked area, 3.8 for total area
clear A output grid TR msk M3d dA
save Cfintuned_large_low_full.mat