C_fin;
% ScenarioCOP = 'small'; % small cop scenario
ScenarioCOP = 'large'; % large cop scenario

% b = 5 * 10^-7; % [mugC ^0.25 s^-1] Metabolic rate coefficient for dormancy 
b = 2.5 * 10^-7; % [mugC ^0.25 s^-1] Metabolic rate coefficient for dormancy 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% Prepare abundances and depths at the right format %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Make base grid
x = -180:1:179; nx = size(x,2); % [degrees] longitude
y = -90:1:90; ny = size(y,2); % [degrees] latitude 
z = 0:100:8000; nz = size(z,2); % [m] depth
dz = 100; % [m] vertical resolution
[xx,yy,zz] = meshgrid(x,y,z);

[XX,YY,ZZ] = meshgrid(X,Y,Z);
%% Reshape abundance data
Fint = interp3(XX,YY,ZZ,F,xx,yy,zz); %  [ind / m3]

x = x([181:end,1:180]);
xx = xx(:,[181:end,1:180],:); xx = mod(xx,360);
yy = yy(:,[181:end,1:180],:);
zz = zz(:,[181:end,1:180],:);
Fint = Fint(:,[181:end,1:180],:);

if strcmp(ScenarioCOP,'small')
    p = 2.1; % [mm]
elseif strcmp(ScenarioCOP,'large')
    p = 2.6;
end
%% Reshape environmental data
load('env_data.mat')
long_env = mod(long_env,360);
long_envmodulo = long_env([181:end,1:180]);
Tempmodulo = Temp([181:end,1:180],:,:);
seafloormodulo = seafloor(:,[181:end,1:180]);

[longenv, latenv,depthenv] = meshgrid(long_envmodulo,lat_env, depth);

temp = interp3(longenv, latenv, depthenv, permute(Tempmodulo,[2 1 3]), xx,yy,zz); % [deg C]
seafl = interp2(longenv(:,:,1), latenv(:,:,1), seafloormodulo, xx(:,:,1), yy(:,:,1)); % [m]

%% Parameters
alpha = 4.2; % [mugC mm^-3] Structural mass coefficient
beta = 7.2; % [mugC mm^-3] Maximum wax ester content coefficient
delta = 0.2; % [-] Minimum wax esther fraction
E = 0.6; % [eV] Activation energy coefficient
k = 8.61733*10^-5; % [eV K^-1] Boltzmann constant
T0 = 273; % [K] Reference temperature 
mu = 0.001; % [day^-1] Mortality rate
Dmax = 330; % [days] Maximum diapause duration

Tcoeff = exp(E*(temp)./(k*T0*(temp+273))); % [-] temperature coefficient

% Respiration rate
r = b * (alpha * p^3)^(3/4) * Tcoeff;   % [mugC s^-1 /individual]

% Structural mass
m =  alpha * p^3; % [mugC] Structural mass of C

% Maximum lipid mass
w = 2.9*p^4.7; % beta * p^3; % [mugC]  Max lipid mass of C

% Diapause length
tC = min((1-delta)*w/(b*Tcoeff*m^(3/4))/24/3600, Dmax); % [days] Diapause length for C, based on Visser et al 2016


%% %%%%%%%%%%%%%%%%%%% Respiration rate %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
R =  r.*Fint .* tC   * 10^-6*3600*24; % [gC / m3 / yr] Respiration rates due to C finmarchicus



Respi = sum(R*dz,3,'omitnan'); % [gC / m2 / yr] Total respiration rates due to all copepods

%% %%%%%%%%%%%%%%%%%%% Mortality rates %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Structural + lipids
muF = mu *  Fint .*  tC  .* (m  + w  - 0.5 * r  .* tC  * 3600 * 24) * 10^-6; % [gC / m3 /yr] Mortality rates for C finmarchicus at each depth

Mort = sum(muF*dz,3,'omitnan'); % [gC / m2 / yr] Total mortality rates due to all copepods

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%% DEGRADATION OF CARCASSES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters
alpha0 = 0.5; % [day^-1] Reference degradation rate at 15 degrees
Tref = 15; % [degrees C] Reference temperature for degradation rate
Q10 = 2; % [-] Q10 coefficient for degradation rate

ALPHA = alpha0 * Q10.^((temp-Tref)/10); % [day^-1] Temperature dependent degradation rate

if strcmp(ScenarioCOP,'small')
    u = 100; % [m/day] Sinking speed of dead copepods
elseif strcmp(ScenarioCOP,'large')
    u = 200; % [m/day] Sinking speed of dead copepods
end

% Concentration of carcasses at each depth
d = zeros(ny,nx,nz); % [gC / m3] Concentration of carcasses at each depth

for dix = 2:size(z,2) % no need to initialize with the depth index = 1 as there should not be any diapausing copepod there. 
    d(:,:,dix) = (muF(:,:,dix)+u*365/dz*d(:,:,dix-1))./ (u*365/dz + ALPHA(:,:,dix)*365); % [gC / m3]
end

% Resulting DIC production at each depth
dic = d .* ALPHA * 365; % [gC / m3 / yr] Production rate of DIC due to carcasses

%Now need to add the sedimenting particles in the computation - we assume
%that they are respired when they reach the seafloor
for i=1:size(x,2)
    for j=1:size(y,2)
        if ~isnan(seafl(j,i))
            idxbottom = find(isnan(squeeze(dic(j,i,:))),1);         
                       
            dic(j,i,idxbottom-1) = dic(j,i,idxbottom-1) + d(j,i,idxbottom-1)*u*365/dz; % added this because we assume that carbon reaching the seafloor is respired
        end
    end
end

%% Compute export
[xq,yq] = meshgrid(x,y);
xq = mod(xq,360);
DLON = 0*xq+1;
DLAT = 0*yq+1;
DX = (2*pi*6371e3/360)*DLON.*cos(deg2rad(yq))*(x(2)-x(1));
DY = (2*pi*6371e3/360)*DLAT*(y(2)-y(1));
Area = DX.*DY; % m^2


expmort =sum(sum( sum(dic,3,'omitnan')*dz.*Area,'omitnan'),'omitnan') /1e15; % [PgC / yr]
exprespi =sum(sum (Respi.*Area,'omitnan'),'omitnan') /1e15; % [PgC/yr]
expmort + exprespi

totbio = sum(sum( sum(Fint,3,'omitnan')*dz.*Area,'omitnan'),'omitnan') /1e15*(w+m)*10^-6; % [PgC] Total biomass at the beginning of diapause

%if we want to compute the area
% xxr = [xx(:,181:end,:)-360,xx(:,1:180,:)];
% yyr = [yy(:,181:end,:),yy(:,1:180,:)];
% mask = interp2(XX(:,:,1),YY(:,:,1),MASK*1,xxr(:,:,1),yyr(:,:,1)); %  [ind / m3]
% sum(sum (mask.*Area,'omitnan'),'omitnan')
% 
% mask2 = Respi>0;
% sum(sum (mask2.*Area,'omitnan'),'omitnan')