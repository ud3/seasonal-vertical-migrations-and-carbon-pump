% addpath C:\Users\jppi\Documents\MATLAB\SVM_sequestration\C_finmarchicus3D\c-grids2

reducearea = 'no'; % test to reduce the area as in the Heath 2004 and Jonasdottir 2015 papers

% First load estimates layer by layer
filen = "200I.GRD";
[matrix200, xmin, xmax, ymin, ymax]=grd_read_v2(filen);
matrix200(matrix200 > 10^35) = NaN;
X = linspace(xmin,xmax,size(matrix200,2));
Y = linspace(ymin,ymax,size(matrix200,1));

filen = "400I.GRD";
[matrix400, ~, ~, ~, ~]=grd_read_v2(filen);
matrix400(matrix400 > 10^35) = NaN;

filen = "600I.GRD";
[matrix600, ~, ~, ~, ~]=grd_read_v2(filen);
matrix600(matrix600 > 10^35) = NaN;

filen = "800I.GRD";
[matrix800, ~, ~, ~, ~]=grd_read_v2(filen);
matrix800(matrix800 > 10^35) = NaN;

filen = "1000I.GRD";
[matrix1000, ~, ~, ~, ~]=grd_read_v2(filen);
matrix1000(matrix1000 > 10^35) = NaN;

filen = "1200I.GRD";
[matrix1200, ~, ~, ~, ~]=grd_read_v2(filen);
matrix1200(matrix1200 > 10^35) = NaN;

filen = "1400I.GRD";
[matrix1400, ~, ~, ~, ~]=grd_read_v2(filen);
matrix1400(matrix1400 > 10^35) = NaN;

filen = "1600I.GRD";
[matrix1600, ~, ~, ~, ~]=grd_read_v2(filen);
matrix1600(matrix1600 > 10^35) = NaN;

filen = "1800I.GRD";
[matrix1800, ~, ~, ~, ~]=grd_read_v2(filen);
matrix1800(matrix1800 > 10^35) = NaN;

filen = "2000I.GRD";
[matrix2000, ~, ~, ~, ~]=grd_read_v2(filen);
matrix2000(matrix2000 > 10^35) = NaN;

filen = "2200I.GRD";
[matrix2200, ~, ~, ~, ~]=grd_read_v2(filen);
matrix2200(matrix2200 > 10^35) = NaN;

filen = "2400I.GRD";
[matrix2400, ~, ~, ~, ~]=grd_read_v2(filen);
matrix2400(matrix2400 > 10^35) = NaN;

filen = "2600I.GRD";
[matrix2600, ~, ~, ~, ~]=grd_read_v2(filen);
matrix2600(matrix2600 > 10^35) = NaN;

filen = "2800I.GRD";
[matrix2800, ~, ~, ~, ~]=grd_read_v2(filen);
matrix2800(matrix2800 > 10^35) = NaN;

filen = "3000I.GRD";
[matrix3000, ~, ~, ~, ~]=grd_read_v2(filen);
matrix3000(matrix3000 > 10^35) = NaN;

% Now collate all abundance maps
F = zeros(size(matrix200,1),size(matrix200,2),15); 
F(:,:,1) = matrix200;
F(:,:,2) = matrix400;
F(:,:,3) = matrix600;
F(:,:,4) = matrix800;
F(:,:,5) = matrix1000;
F(:,:,6) = matrix1200;
F(:,:,7) = matrix1400;
F(:,:,8) = matrix1600;
F(:,:,9) = matrix1800;
F(:,:,10) = matrix2000;
F(:,:,11) = matrix2200;
F(:,:,12) = matrix2400;
F(:,:,13) = matrix2600;
F(:,:,14) = matrix2800;
F(:,:,15) = matrix3000;

if strcmp(reducearea,'yes')
    load mask_finmarchicus.mat
    F = F.*MASK;
end

abund = sum(F,3,'omitnan'); % [# / m2] Abundance of C finmarchicus
abund(abund == 0 ) = NaN; 
F = F/ 200; % [# / m3] Divide by the width of the water layer to have concentrations in # / m3
Z = [100 300 500 700 900 1100 1300 1500 1700 1900 2100 2300 2500 2700 2900]; % [m] Centre of the vertical bins 
%% Plot abundances
figure
ax = axesm('mollweid','MapProjection','mercator','Frame','on','MapLonLimit',[-60 10],'MapLatLimit', [53 68.5],'Origin', [0 -160 0],'FLineWidth',0.5);
surfm(Y,X,abund,'AlphaData',~isnan(abund)), colorbar
box off
axis off
load coast
geoshow(lat, long,'Color','k')

ax.XTick = [-60 -30 0];
ax.YTick = [53 60 67];
colormap('jet')
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);