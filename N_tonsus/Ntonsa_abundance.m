%% Extract and draw the limits of the south tropical front and the antarctic convergence
A = readtable('STF.csv');
y_stf = -(90 - table2array(A(:,1)));
x_stf = table2array(A(:,2));

A = readtable('AC.csv');
y_ac = -(90 - table2array(A(:,1)));
x_ac = table2array(A(:,2));

%% Make base grid
x = 0:1:359; nx = size(x,2); % [degrees] longitude 
y = -90:1:90; ny = size(y,2); % [degrees] latitude 
z = 0:100:8000; nz = size(z,2); % [m] depth
dz = 100; % [m] vertical resolution
[xx,yy,zz] = meshgrid(x,y,z);

%% Create the shapes of the 3 zones
Zone1 = zeros(nx,ny); %between AC and 2 degrees south of STF
Zone2 = zeros(nx,ny); %between STF and 2 degrees south of STF
Zone3 = zeros(nx,ny); %between STF and 5 degrees north of STF

stf = zeros(nx,ny); ac = zeros(nx,ny);
for i=1:nx
    [~,id] = min(abs(x_stf-x(i)));
    [~,idy] = min(abs(y-y_stf(id)));
    stf(i,idy) = 1;
    
    [~,id] = min(abs(x_ac-x(i)));
    [~,idy] = min(abs(y- y_ac(id)));
    ac(i,idy) = 1;
end

[~,YSTF] = max(stf'); YSTF = y(YSTF); YSTF(308) = -43; %last bit just to correct an interpolating bug
[~,YAC] = max(ac'); YAC = y(YAC);
 
for i=1:nx
    for j=1:ny
        if yy(j,i,1) > YAC(i) && yy(j,i,1) <= (YSTF(i)-2)
            Zone1(i,j) = 1;
        elseif yy(j,i,1) > (YSTF(i)-2) && yy(j,i,1) <= YSTF(i)
            Zone2(i,j) = 1;
        elseif yy(j,i,1) > YSTF(i) && yy(j,i,1) <= (YSTF(i)+5)
            Zone3(i,j) = 1;
        end
    end
end


%Remove land areas
xx2 = mod(xx(:,:,1),360); xx2(xx2>180) = xx2(xx2>180)-360;
land = landmask(yy(:,:,1),xx2);

Zone1(land'==1)=0; Zone2(land'==1)=0; Zone3(land'==1)=0;


%% Create abundance map
Z1 = 5014; % [ind / m2] Abundance of individuals per m2 in zone 1
Z2 = 27195; % [ind / m2] Abundance of individuals per m2 in zone 2
Z3 = 10000; % [ind / m2] Abundance of individuals per m2 in zone 3

Tonsa = Z1*Zone1 + Z2*Zone2 + Z3*Zone3; % [ind / m2]


Tonsa(Tonsa==0) = NaN;
Tonsa = Tonsa';

Tonsa(:, [1:147, 206:360]) = NaN; %constrain to keep only below NZ, ie between 147 and -156 degrees
%% Plot abundances
figure
ax = axesm('eqdazim','Frame','on','MapLatLimit',[-90 -10],'Origin', [-90 0 0],'FLineWidth',0.5);
ax.XTick = [-120 -60 0 60 120 180];
ax.YTick = [-40 -20 0 20 40];
box off
axis off
load coast
% geoshow(lat, long,'Color','k')

hold on
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);

surfm(yy(:,:,1),xx(:,:,1),Tonsa,'AlphaData',~isnan(Tonsa))
% plotm(y_stf,x_stf,'r')
% plotm(y_ac,x_ac,'k')
colormap('jet')
colorbar
