Latitude = -[46 43.5 41.5 42.5 42.5 42+25/60 45 33 45.5 37 40 50 43 40 43 51.5 45.5 40 55];
Longitude = [174 176.5 179 170 170.5 173+49/60 -157-3/60 44 171 99 121 -75 147.5 -25 -25 -25 -126 -126 -158];

Ntonsusrecords = [Longitude; Latitude];

line1x = [-156 -156];
line1y = [-80 -31];

line2x = [147 147];
line2y = [-80 -31];

figure
ax = axesm('eqdazim','Frame','on','MapLatLimit',[-90 -30],'Origin', [-90 0 0],'FLineWidth',0.5);
% eqdazim
box off
axis off
load coast
scatterm(Latitude, Longitude, 6)
geoshow('landareas.shp', 'FaceColor', [0.5 0.5 0.5]);
hold on
plotm(line1y, line1x, 'k')
plotm(line2y, line2x, 'k')
title('Presence of N tonsus')